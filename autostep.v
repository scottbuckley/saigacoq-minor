
(** Saiga's autostep tactic **)

(* This file defines some helpful theorems for using Saiga, as well as some
   tacticals, particularly "autostep", which automates the proof of one defined
   expression stepping into another *)

Require Import Coq.Arith.Arith.
Require Import Coq.Bool.Bool.
Require Import Coq.Lists.List.
Require Export saiga.

Delimit Scope string_scope with string.
Local Open Scope string_scope.

Require Import Coq.Program.Equality.

(* Below are a number of helpful lemmas which are
   leveraged in autostep. *)

Require Import ProofIrrelevance.
Lemma eq_dec_left: forall {T:Type},
  type_eq_dec T T = left (eq_refl T).
Proof.
  intros. destruct (type_eq_dec T T).
  enough (e = eq_refl); subst; auto.
  apply proof_irrelevance.
  contradiction n; auto.
Defined.


Lemma beq_node_refl: forall (n:node),
  beq_node n n = true.
Proof.
  intros. destruct n. simpl. destruct n. simpl.
  repeat rewrite <- beq_nat_refl. reflexivity.
Qed.


Lemma beq_ascii_refl: forall (a:ascii),
  beq_ascii a a = true.
Proof.
  intros. destruct a. simpl. repeat (rewrite eqb_reflx). auto.
Qed.


Lemma beq_ascii_true_iff: forall (a b:ascii),
  beq_ascii a b = true -> a = b.
Proof.
  intros. generalize dependent b.
  induction a; intros. destruct b7.
  - inversion H. repeat (rewrite andb_true_iff in H1).
    expand_ands. rewrite eqb_true_iff in *. subst. auto.
Qed.

Lemma beq_string_refl: forall (a:string),
  beq_string a a = true.
Proof.
  intros. induction a.
  - auto.
  - simpl. rewrite IHa. rewrite beq_ascii_refl. auto.
Qed.

Lemma beq_string_true_iff: forall (a b:string), 
  beq_string a b = true -> a = b.
Proof.
  intros. generalize dependent b.
  induction a; intros; destruct b; auto.
  - simpl in H. congruence.
  - simpl in H. congruence.
  - simpl in H. rewrite andb_true_iff in H. expand_ands.
    apply IHa in H1. apply beq_ascii_true_iff in H0. subst. auto.
Qed.

Lemma beq_any_string: forall (a b:string),
  beq_any a b = beq_string a b.
Proof.
  intros.
  destruct (beq_any a b) eqn:HH.
  - unfold beq_any in HH. rewrite eq_dec_left in HH.
    rewrite conv_eq in HH. destruct (all_eq_dec a b) eqn:HHH.
    + subst. rewrite beq_string_refl. auto.
    + congruence.
  - unfold beq_any in HH. rewrite eq_dec_left in HH.
    rewrite conv_eq in HH. destruct (all_eq_dec a b) eqn:HHH.
    + congruence.
    + destruct (beq_string a b) eqn:Hs; auto.
      apply beq_string_true_iff in Hs. congruence.
Qed.


Theorem beq_attr_refl: forall {P T:Type} (a:attr P T),
  beq_attr a a = true.
Proof.
  intros.
  destruct a. unfold beq_attr. simpl.
  repeat rewrite eq_dec_left. rewrite conv2_eq.
  repeat rewrite <- beq_nat_refl. rewrite eqb_reflx.
  simpl. apply beq_any_refl.
Qed.

Theorem beq_all_refl: forall {T:Type} (t:T),
  beq_any t t = true.
Proof.
  intros. unfold beq_any. destruct (type_eq_dec T T); auto.
  rewrite conv_eq. destruct (all_eq_dec t t); auto.
Qed.

Lemma beq_any_pair: forall {A B:Type} (a1 a2:A) (b1 b2:B),
  beq_any (a1, b1) (a2, b2) = beq_any a1 a2 && beq_any b1 b2.
Proof.
  intros.
  destruct (beq_any (a1, b1) (a2, b2)) eqn:HH.
  all: unfold beq_any in HH; rewrite eq_dec_left in HH; rewrite conv_eq in HH.
  - destruct (all_eq_dec (a1, b1) (a2, b2)) eqn:HHH.
    inversion e. subst. repeat rewrite beq_all_refl. auto. congruence.
  - destruct (all_eq_dec (a1, b1) (a2, b2)) eqn:HHH. congruence.
    destruct (beq_any a1 a2 && beq_any b1 b2) eqn:Hs; auto.
    rewrite andb_true_iff in Hs. expand_ands. unfold beq_any in *.
    rewrite eq_dec_left in H. rewrite eq_dec_left in H0.
    rewrite conv_eq in H. rewrite conv_eq in H0. contradiction n.
    destruct (all_eq_dec a1 a2); destruct (all_eq_dec b1 b2); subst; auto; congruence.
Qed.

Fixpoint beq_listnode (l1 l2:list node) : bool :=
  match l1, l2 with
    | h1::r1, h2::r2 => beq_node h1 h2 && beq_listnode r1 r2
    | nil, nil       => true
    | _, _           => false
  end.

Lemma beq_listnode_refl: forall (l:list node),
  beq_listnode l l = true.
Proof.
  intros. induction l; auto.
  simpl. rewrite beq_node_refl. simpl. auto. Qed.

Lemma beq_nkind_true_iff: forall (nk1 nk2:nkind),
  beq_nkind nk1 nk2 = true <-> nk1 = nk2.
Proof.
  split; intros H.
  - destruct nk1; destruct nk2. simpl in *. rewrite beq_nat_true_iff in H. subst; auto.
  - subst. destruct nk2. simpl. rewrite <- beq_nat_refl. auto.
Qed.

Lemma beq_node_true_iff: forall (n1 n2:node),
  beq_node n1 n2 = true <-> n1 = n2.
Proof.
  split; intros H.
  - destruct n1; destruct n2. simpl in H. rewrite andb_true_iff in H. expand_ands.
    rewrite beq_nkind_true_iff in H0. rewrite beq_nat_true_iff in H1. subst. auto.
  - subst. apply beq_node_refl.
Qed.

Lemma beq_listnode_true_iff: forall (l1 l2:list node),
  beq_listnode l1 l2 = true <-> l1 = l2.
Proof.
  split; intros H.
  - generalize dependent l2. induction l1; intros l2 H.
    + destruct l2; inversion H; auto.
    + destruct l2; inversion H; auto. rewrite andb_true_iff in H1. expand_ands.
      rewrite beq_node_true_iff in H0. apply IHl1 in H2. subst; auto.
  - subst. induction l2; auto. simpl. rewrite beq_node_refl. rewrite IHl2. auto.
Qed.

Lemma beq_any_listnode: forall (l1 l2:list node),
  beq_any l1 l2 = beq_listnode l1 l2.
Proof.
  intros.
  destruct (beq_any l1 l2) eqn:HH.
  - unfold beq_any in HH. rewrite eq_dec_left in HH.
    rewrite conv_eq in HH. destruct (all_eq_dec l1 l2) eqn:HHH.
    + subst. rewrite beq_listnode_refl. reflexivity.
    + congruence.
  - unfold beq_any in HH. rewrite eq_dec_left in HH.
    rewrite conv_eq in HH. destruct (all_eq_dec l1 l2) eqn:HHH.
    + congruence.
    + destruct (beq_listnode l1 l2) eqn:Hs; auto.
      apply beq_listnode_true_iff in Hs. congruence.
Qed.






(* Below are a number of helper tacticals *)

(* 'cleanup' converts some expressions into simpler expressions,
   using proofs of equivalence. *)
Ltac cleanup :=
  repeat (rewrite eq_dec_left);
  repeat (rewrite conv_eq);
  repeat (rewrite beq_all_refl);
  repeat (rewrite beq_any_pair);
  repeat (rewrite beq_any_string).

(* 'auto_ns' performs auto, unless the goal is a step or multistep. This is used
   to avoid the lengthy 'auto' process on step relations, which usually fails anyway *)
Ltac auto_ns :=
  repeat (rewrite conv_eq);
  match goal with
    | |- (_ ~ _ |~ _                    ~~>* _ |~ _) => idtac
    | |- (_ ~ _ |~ _                    ~~>  _ |~ _) => idtac
    | |- _                                           => auto
  end.

(* 'ctx_simpl' is similar to 'cleanup', but performs a few more actions *)
Ltac ctx_simpl :=
  cbn;
  repeat (rewrite conv_eq);
  repeat (rewrite eq_dec_left);
  repeat (rewrite beq_attr_refl);
  repeat (rewrite beq_all_refl);
  repeat (rewrite beq_any_pair);
  repeat (rewrite beq_any_string);
  cbn.

Hint Unfold applycontext.
Hint Unfold context_baseline.

(* sometimes notations help with pattern matching *)
Notation "c '++' a '//' af" := (replace_afun c a af) (at level 36, no associativity).
Notation "c '++' n '\\' a '\\' p '\\' t" := (replace_node c n a p t) (at level 36, no associativity).

(* 'break_ctx' applies cbn after first hiding some content *)
Ltac break_ctx :=
  match goal with
    | |- context [(?ctx ++ ?n \\ ?a \\ ?p \\ ?e) << _] => remember ctx as ctemp eqn:Htemp; cbn; subst
  end.

(* some other simple tacticals *)
Ltac clean_ctx :=
  repeat (rewrite eq_dec_left);
  repeat (rewrite beq_any_listnode);
  simpl.

Ltac simplify_ctx :=
  repeat (break_ctx; clean_ctx).

Ltac step_cleanup := 
  ctx_simpl; autounfold; simpl; auto_ns.

(* 'autostep_match' applies various constructors and other tactics, depending on the
   content of the goal. It also outputs details to the console. *)
Ltac autostep_match :=
  match goal with

    (* multi-step, or finished *)
    | |- (_ ~ _ |~ _                    ~~>* _ |~ _)     => (eapply multi_refl;        idtac "solved by reflection")
                                                          || (eapply multi_step;        idtac "applied multi_step")

    (* attribute access *)
    | |- (_~ _  |~ (eVal _) DOT ?a WITH (eVal ?p) ~~> _ |~ _)  => (idtac "<a>" a; idtac "<p>" p; eapply st_attrapp_uncached; simplify_ctx; auto;
                                                                   (left; tauto) || (right; left; tauto) ||
                                                                   (right; right; ctx_simpl; tauto);
                                                                   (idtac "applied st_attrapp_uncached"))
                                                             || (apply st_attrapp_cached; simplify_ctx; auto;
                                                                    split; auto; ctx_simpl; auto;
                                                                   (idtac "applied st_attrapp_cached"))

    | |- (_~ _ |~ (eVal _)  DOT _ WITH _          ~~> _ |~ _)        => eapply st_attr_parstep;       idtac "applied st_attr_parstep"

    | |- (_~ _ |~ _      DOT _ WITH _          ~~> _ |~ _)        => eapply st_attr_nodestep;       idtac "applied st_attr_nodestep"

    (* conditional *)
    | |- (_~ _ |~ eCond (eVal _) _ _     ~~> _ |~ _)     => eapply st_condapp;        idtac "applied st_condapp"
    | |- (_~ _ |~ eCond _ _ _            ~~> _ |~ _)     => eapply st_condleft;       idtac "applied st_condleft"

    | |- (_~ _ |~ (eVal _) OF  (eVal _)  ~~> _ |~  _)    => eapply st_appapp;         idtac "applied st_appapp"
    | |- (_~ _ |~ (eVal _) OF  _         ~~> _ |~  _)    => eapply st_appright;       idtac "applied st_appright"
    | |- (_~ _ |~ _        OF  _         ~~> _ |~  _)    => eapply st_appleft;        idtac "applied st_appleft"

    (* cache *)
    | |- (_~ _ |~ _/_/_ ;= eVal _        ~~> _ |~  _)    => eapply st_cachewrite;  idtac "applied st_cachewrite"
    | |- (_~ _ |~ _/_/_ ;= _             ~~> _ |~  _)    => eapply st_cacheleft;   idtac "applied st_cacheleft"

    (* retrieving *)
    | |- conv _ _         => rewrite conv_eq
  end.

(* 'autostep' performs some simplifications before and after applying 'autostep_match' *)
Ltac autostep :=
  step_cleanup; autostep_match; step_cleanup.

(* 'autostep_lg' (large) attempts to prove the first step in a multistep sequence, including
   all nested step proofs that are required. *)
Ltac autostep_lg := ((eapply multi_refl) || (eapply multi_step; idtac "applied multi_step"; [repeat (autostep)|])); simpl; step_cleanup; step_cleanup.

(* 'autostep_all' performs steps as long as it can, counting how many
   steps it took to reach a value (if it can) *)
Ltac autostep_all_n n := (idtac n; autostep_lg; [autostep_all_n (S n)]) || (idtac "finished in" n "steps."; autostep_lg).
Ltac autostep_all := autostep_all_n 0.







