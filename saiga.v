
(** Saiga **)

(* This file defines Saiga's core functionality and proves some very basic properties *)

Require Import Coq.Arith.Arith.
Require Import Coq.Bool.Bool.
Require Import Coq.Lists.List.
Require Export Ascii.
Require Export String.
Require Import Eqdep.
Require Import Coq.Program.Equality.

Delimit Scope string_scope with string.
Local Open Scope string_scope.


(* Axioms we use (similar to excluded middle) *)

Axiom type_eq_dec: forall (T1 T2:Type),
  {T1=T2}+{T1<>T2}.

Axiom all_eq_dec: forall {T:Type} (v1 v2:T),
  {v1=v2}+{v1<>v2}.

Lemma all_eq_dec_irrefutable: forall {T:Type} (v1 v2:T), (({v1=v2}+{v1<>v2}) -> False) -> False.
Proof. intros. apply H. right. intros HH. apply H. left. apply HH. Qed.

Lemma type_eq_dec_irrefutable: forall (v1 v2:Type), (({v1=v2}+{v1<>v2}) -> False) -> False.
Proof. intros. apply H. right. intros HH. apply H. left. apply HH. Qed.



(*** High level structures ***)

(* convenience functions for using evidence of type equality to shift btwn types *)
Definition conv {A:Type} {a b:A} {Tfun:A -> Type} (ev:a=b) (pa:Tfun b) : Tfun a :=
  eq_rect b Tfun pa a (eq_sym ev).

Definition conv2 {A B:Type} {a a':A} {b b':B} {Tfun:A -> B -> Type} (ev1:a=a') (ev2:b=b') (pa:Tfun a' b') : Tfun a b :=
  eq_rect_r (fun a0 : A => Tfun a0 b)
  (eq_rect_r (Tfun a') pa ev2) ev1.

Lemma conv_eq: forall {T:Type} {Tf:T->Type} (p:T) (e:p=p) (v:Tf p),
  conv e v = v.
Proof.
  intros. symmetry. unfold conv. unfold eq_rec_r. apply eq_rect_eq.
Defined.

Lemma conv2_eq: forall {A B:Type} {Tf:A->B->Type} (a:A) (b:B) (e1:a=a) (e2:b=b) (v:Tf a b),
  conv2 e1 e2 v = v.
Proof.
  intros. symmetry. unfold conv2. unfold eq_rect_r. rewrite <- eq_rect_eq. apply eq_rect_eq.
Qed.



(* needed once or twice for comparing things of unknown types *)
Definition beq_any {T T':Type} (t:T) (t':T') : bool :=
  match type_eq_dec T T' with
    | left ev => if all_eq_dec t (conv ev t') then true else false
    | right _ => false
  end.

Lemma beq_any_refl: forall {T:Type} (t:T),
  beq_any t t = true.
Proof. intros. unfold beq_any. destruct (type_eq_dec T T).
  rewrite conv_eq. destruct (all_eq_dec t t); auto. auto.
Qed.




(*** Tacticals ***)
Ltac simpl_existT :=
  match goal with
    [ H : existT _ ?x _ = existT _ ?x _ |- _ ] =>
    let Hi := fresh H in assert(Hi:=inj_pairT2 _ _ _ _ _ H) ; clear H
  end.

Ltac substT2 := (* solves dependent equalities and substitutes *)
  repeat (simpl_existT; try (subst)).

Ltac solve_by_inverts n :=
  match goal with | H : ?T |- _ => 
  match type of T with Prop =>
    solve [ 
      inversion H;
      match n with S (S (?n')) => subst; solve_by_inverts (S n') end ]
  end end.

Ltac try_solve_invert :=
  try (solve_by_inverts 1).

Ltac expand_ands :=
  repeat (
  match goal with
    | H:(_ /\ _) |- _   => decompose [and] H; clear H
  end).

Ltac exex := repeat (eexists).

Notation "[ x ; .. ; y ]" := (cons x .. (cons y nil) ..).





(*** Basic Inductive Types ***)
(* A node has a 'kind', which is a simple enumarated type. The rest of the
  structures below are described in the paper *)

(* nat (unique id) *)
Inductive nkind : Type :=
  | NKind : nat -> nkind.

(* nkind (node kind), nat (unique id) *)
Inductive node : Type :=
  | Node : nkind -> nat -> node.

(* type (parameter type), type (return type), T (defualt value)
   bool (caching switch), nat (unique id) *)
Inductive attr : Type -> Type -> Type :=
  | Attr : forall {P T:Type}, T -> bool -> nat -> attr P T.

(* NB: attr needs a default value to make the context a total function *)

Hint Constructors nkind node attr.

(*** Simple functions ***)

Definition cached {P T:Type} (a:attr P T) : bool :=
  match a with
    | Attr _ b _ => b
  end.

Definition attr_default {P T:Type} (a:attr P T) : T :=
  match a with
    | Attr d _ _ => d
  end.

(* (only) check if two attribute have the same unique id *)
Definition neq_attr {P1 P2 T1 T2:Type} (a1:attr P1 T1) (a2:attr P2 T2) : bool :=
  match a1, a2 with
    | Attr _ _ n1, Attr _ _ n2 => beq_nat n1 n2
  end.

(* compare two attributes of the same type(s) *)
Definition beq_attr_same {P T:Type} (a1:attr P T) (a2:attr P T) : bool :=
  match a1, a2 with
    | Attr d1 b1 n1, Attr d2 b2 n2 => beq_nat n1 n2 && eqb b1 b2 && beq_any d1 d2
  end.

Definition beq_attr {P1 P2 T1 T2:Type} (a1:attr P1 T1) (a2:attr P2 T2) : bool :=
  neq_attr a1 a2 &&
  match type_eq_dec P1 P2, type_eq_dec T1 T2 with
    | left evP, left evT => beq_attr_same a1 (conv2 evP evT a2)
    | _, _ => false
  end.

Definition beq_nkind (k1 k2 : nkind) : bool :=
  match k1, k2 with
    | NKind n1, NKind n2 => beq_nat n1 n2
  end.

Definition beq_node (n1 n2 : node) : bool :=
  match n1, n2 with
    | Node k1 n1, Node k2 n2 => beq_nkind k1 k2 && beq_nat n1 n2
  end.

(* get a node's kind *)
Definition node_nk (n:node) : nkind :=
  match n with
    | Node nk _ => nk
  end.

Definition isnk (n:node) (nk:nkind) :=
  beq_nkind (node_nk n) nk.

(* flip the above *)
Definition isnk2 (nk:nkind) (n:node) :=
  beq_nkind (node_nk n) nk.

Definition beq_ascii (a1 a2:ascii) : bool :=
  match a1, a2 with
    Ascii a b c d e f g h, Ascii a' b' c' d' e' f' g' h' =>
      eqb a a' && eqb b b' && eqb c c' && eqb d d' &&
      eqb e e' && eqb f f' && eqb g g' && eqb h h'
  end.

Fixpoint beq_string (s1 s2:string) : bool :=
  match s1, s2 with
    | String h1 r1, String h2 r2 => beq_ascii h1 h2 && beq_string r1 r2
    | EmptyString, EmptyString => true
    | _, _ => false
  end.





(*|
| |                                    _
| |   _____  ___ __  _ __ ___  ___ ___(_) ___  _ __
| |  / _ \ \/ / '_ \| '__/ _ \/ __/ __| |/ _ \| '_ \
| | |  __/>  <| |_) | | |  __/\__ \__ \ | (_) | | | |
| |  \___/_/\_\ .__/|_|  \___||___/___/_|\___/|_| |_|
| |           |_|
|*)


(* This is the actual expression type, as shown in the paper *)
Inductive exp: Type -> Type :=
  | eVal  : forall {T:Type}, T -> exp T
  | eCond : forall {T:Type}, exp bool -> exp T -> exp T -> exp T
  | eApp  : forall {T1 T2:Type}, exp (T1 -> T2) -> exp T1 -> exp T2
  | eAttr : forall {P T:Type}, exp node -> attr P T -> exp P -> exp T
  | eCache: forall {P T:Type}, node -> attr P T -> P -> exp T -> exp T.
Hint Constructors exp.

(* Notations *)
Notation "'IFF' a 'THEN' b 'ELSE' c" := (eCond a b c)         (at level 40).
Notation "f 'OF' p"                  := (eApp f p)            (at level 40, left associativity).
Notation "n 'DOT' a"                 := (eAttr n a (eVal tt)) (at level 40, no associativity).
Notation "n 'DOT' a 'WITH' p"        := (eAttr n a p)         (at level 40, no associativity, a at next level).
Notation "n '/' a '/' p ';=' e"      := (eCache n a p e)      (at level 40, no associativity, a at next level).

(* value definition, both functional and propositional *)
Inductive value: forall {T:Type}, exp T -> Prop :=
  | ValVal : forall {T:Type} (v:T), value (eVal v).
Hint Constructors value.

Definition isVal {T:Type} (e:exp T) : bool :=
  match e with
    | eVal _ => true
    | _      => false
  end.
Hint Unfold isVal.

Lemma value__isVal: forall {T:Type} (e:exp T), isVal e = true <-> value e.
Proof. induction e; split; intros; auto; inversion H. Qed.




(*|
| |                 _            _
| |  ___ ___  _ __ | |_ _____  _| |_
| | / __/ _ \| '_ \| __/ _ \ \/ / __|
| || (_| (_) | | | | ||  __/>  <| |_
| | \___\___/|_| |_|\__\___/_/\_\\__|
| |
| |
|*)

(* the actual definition of a context *)
Definition context: Type := forall {P T:Type}, node -> attr P T -> P -> exp T.

(* applycontext (<<) is doesn't really do anything but is helpful for pattern matching *)
Definition applycontext (ctx:context) (P T:Type) (n:node) (a:attr P T) (p:P) : exp T :=
  ctx P T n a p.

(* a context function that only uses defaults *)
Definition context_baseline : context :=
  fun P' T':Type => fun n':node => fun a':attr P' T' => fun p':P' =>
    eVal (attr_default a').

(* a wrapper that 'adds' a cache value to a context *)
Definition replace_node {P T:Type} (ctx:context) (n:node) (a:attr P T) (p:P) (v:T) : context :=
  fun P':Type  => fun T':Type =>
   fun n':node => fun a':attr P' T' => fun p':P' =>
    if neq_attr a a' && beq_node n n' then
      match type_eq_dec P' P, type_eq_dec T' T with
        | left evP, left evT => if beq_attr a a' && beq_any p p'
                              then conv evT (eVal v)
                              else applycontext ctx P' T' n' a' p'
        | _,_ => applycontext ctx P' T' n' a' p'
      end
    else applycontext ctx P' T' n' a' p'.

Definition replace_node_tuple {P T:Type} (ctx:context) (napv:node * attr P T * P * T) : context :=
  let (nap, v) := napv in let (na, p) := nap in let (n, a) := na in
  replace_node ctx n a p v.


(* a wrapper that 'adds' a full attribute definition to a context *)
Definition replace_afun {P T:Type} (ctx:context) (a:attr P T) (af:node -> P -> exp T) : context :=
  fun P':Type => fun T':Type =>
  fun n':node => fun a':attr P' T' => fun p':P' =>
    if neq_attr a a' then
      match type_eq_dec P P', type_eq_dec T' T with
        | left evP, left evT => if beq_attr a a'
                              then conv evT (af n' (conv evP p'))
                              else applycontext ctx P' T' n' a' p'
        | _,_ => applycontext ctx P' T' n' a' p'
      end
    else applycontext ctx P' T' n' a' p'.

(* notations *)
Notation "c '^^' napv" := (replace_node_tuple c napv) (at level 40).
Notation "ctx '<<' a b c d e" := (applycontext ctx a b c d e) (at level 10, no associativity).
Notation "ctx '<<' a" := (applycontext ctx a) (at level 10, no associativity, only parsing).




(*|
| |      _
| |  ___| |_ ___ _ __
| | / __| __/ _ \ '_ \
| | \__ \ ||  __/ |_) |
| | |___/\__\___| .__/
| |             |_|
|*)

(* Here we inductively define the 'step' relation, and some variants *)

(*
  ~~> is the default step (cached/uncached needs to be specified)
  --> is uncached step
  ==> is cached step

  ~~>* is multistep
  -->* is uncached multistep
  ==>* is cached multistep

  ~~~> is bigstep (big step to a value, defined independently of step, but
                   proven equivalent to multistep (proof not shown here))
*)

Reserved Notation "b '~' c1 '|~' e1 '~~>' c2 '|~' e2" (at level 60).

Program Inductive step : forall {T:Type}, bool -> context -> exp T -> context -> exp T -> Prop :=
  (* Cond *)
  | st_condleft: forall (b:bool) {T:Type} (ctx ctx':context) (e1 e1':exp bool) (e2 e3:exp T),
      b~ ctx |~ e1 ~~> ctx' |~ e1' ->
        b~ ctx |~ eCond e1 e2 e3 ~~> ctx' |~ eCond e1' e2 e3

  | st_condapp: forall (b:bool) {T:Type} (ctx:context) (e2 e3:exp T) (b':bool),
      b~ ctx |~ eCond (eVal b') e2 e3 ~~> ctx |~ if b' then e2 else e3

  (*App*)
  | st_appleft: forall (b:bool) {T1 T2:Type} (ctx ctx':context) (e e':exp (T1 -> T2)) (e2:exp T1),
      b~ ctx |~ e ~~> ctx' |~ e' ->
        b~ ctx |~ eApp e e2 ~~> ctx' |~ eApp e' e2

  | st_appright: forall (b:bool) {T1 T2:Type} (ctx ctx':context) (e e':exp T1) (f:T1 -> T2),
      b~ ctx |~ e ~~> ctx' |~ e' ->
        b~ ctx |~ eApp (eVal f) e ~~> ctx' |~ eApp (eVal f) e'

  | st_appapp: forall (b:bool) {T1 T2:Type} (ctx:context) (v:T1) (f: T1 -> T2),
        b~ ctx |~ eApp (eVal f) (eVal v) ~~> ctx |~ eVal (f v)

  (*Attr*)
  | st_attr_nodestep: forall (b:bool) {P T:Type} (ctx ctx':context) (e1 e1':exp node) (e2:exp P) (a:attr P T),
      b~ ctx |~ e1 ~~> ctx' |~ e1' ->
        b~ ctx |~ eAttr e1 a e2 ~~> ctx' |~ eAttr e1' a e2

  | st_attr_parstep: forall (b:bool) {P T:Type} (ctx ctx':context) (n:node) (e e':exp P) (a:attr P T),
      b~ ctx |~ e ~~> ctx' |~ e' ->
        b~ ctx |~ eAttr (eVal n) a e ~~> ctx' |~ eAttr (eVal n) a e'

  | st_attrapp_uncached: forall (b:bool) {P T:Type} (ctx:context) (n:node) (p:P) (a:attr P T) (e:exp T),
      e = applycontext ctx P T n a p ->
        cached a = false \/ b = false \/ isVal e = true ->
          b~ ctx |~ eAttr (eVal n) a (eVal p) ~~> ctx |~ e

  | st_attrapp_cached: forall {P T:Type} (ctx:context) (n:node) (p:P) (a:attr P T) (e:exp T),
      e = applycontext ctx P T n a p ->
        cached a = true /\ isVal e = false ->
          true~ ctx |~ eAttr (eVal n) a (eVal p) ~~> ctx |~ (n / a / p ;= e)

  (*eCache*)
  | st_cacheleft: forall (b:bool) {P T:Type} (ctx ctx':context) (e e':exp T) (n:node) (p:P) (a:attr P T),
      b~ ctx |~ e ~~> ctx' |~ e' ->
        b~ ctx |~ n / a / p ;= e ~~> ctx' |~ n / a / p ;= e'

  | st_cachewrite: forall (b:bool) {P T:Type} (ctx:context) (n:node) (p:P) (a:attr P T) (v:T),
      b~ ctx |~ n / a / p ;= eVal v ~~> ctx ^^ (n, a, p, v) |~ eVal v

where "b '~' c1 '|~' e1 '~~>' c2 '|~' e2" := (step b c1 e1 c2 e2).
Hint Constructors step.

Notation "c1 '|=' e1 '==>' c2 '|=' e2" := (step true  c1 e1 c2 e2) (at level 60, no associativity, c2 at next level).
Notation "c1 '||-' e1 '-->' c2 '||-' e2" := (step false c1 e1 c2 e2) (at level 60, no associativity, c2 at next level).


(*** MULTI STEP ***)
Inductive multistep : forall {T:Type}, bool -> context -> exp T -> context -> exp T -> Prop :=
  | multi_refl : forall (b:bool) {T:Type} (ctx:context) (x:exp T),
                  multistep b ctx x ctx x
  | multi_step : forall (b:bool) {T:Type} (ctx ctx' ctx'':context) (x y z:exp T),
                       step b ctx  x ctx'  y ->
                  multistep b ctx' y ctx'' z ->
                  multistep b ctx  x ctx'' z.
Hint Constructors multistep.

Notation "b '~' c1 '|~' e1 '~~>*' c2 '|~' e2"   := (multistep b     c1 e1 c2 e2) (at level 60, no associativity, c2 at next level).
Notation       "c1 '|=' e1 '==>*' c2 '|=' e2"   := (multistep true  c1 e1 c2 e2) (at level 60, no associativity, c2 at next level).
Notation       "c1 '||-' e1 '-->*' c2 '||-' e2" := (multistep false c1 e1 c2 e2) (at level 60, no associativity, c2 at next level).


Reserved Notation "b '~' c1 '|~' e1 '~~~>' c2 '|~' e2" (at level 60).
Inductive bigstep : forall {T:Type}, bool -> context -> exp T -> context -> exp T -> Prop :=
  (* Refl *)
  | bst_refl: forall (b:bool) {T:Type} (ctx:context) (v:T),
      b~ ctx |~ eVal v ~~~> ctx |~ eVal v

  (* Cond *)
  | bst_condtrue: forall (b:bool) {T:Type} (ctx ctx' ctx'':context) (e1:exp bool) (e2 e3:exp T) (v:T),
      b~ ctx  |~ e1 ~~~> ctx'  |~ eVal true ->
      b~ ctx' |~ e2 ~~~> ctx'' |~ eVal v ->
        b~ ctx |~ eCond e1 e2 e3 ~~~> ctx'' |~ eVal v

  | bst_condfalse: forall (b:bool) {T:Type} (ctx ctx' ctx'':context) (e1:exp bool) (e2 e3:exp T) (v:T),
      b~ ctx  |~ e1 ~~~> ctx'  |~ eVal false ->
      b~ ctx' |~ e3 ~~~> ctx'' |~ eVal v ->
        b~ ctx |~ eCond e1 e2 e3 ~~~> ctx'' |~ eVal v

  (* App *)
  | bst_app: forall (b:bool) {T1 T2:Type} (ctx ctx' ctx'':context) (e1:exp (T1 -> T2)) (v1:T1->T2) (e2:exp T1) (v2:T1),
      b~ ctx  |~ e1 ~~~> ctx'  |~ eVal v1 ->
      b~ ctx' |~ e2 ~~~> ctx'' |~ eVal v2 ->
        b~ ctx |~ eApp e1 e2 ~~~> ctx'' |~ eVal (v1 v2)

  (* Attr *)
  | bst_attr_cached: forall {P T:Type} (ctx ctx' ctx'' ctx''':context) (e1:exp node) (n:node) (e2:exp P) (p:P) (a:attr P T) (e3:exp T) (v:T),
      applycontext ctx'' P T n a p = e3 ->
      cached a = true /\ isVal e3 = false ->
        true~ ctx   |~ e1 ~~~> ctx'   |~ eVal n ->
        true~ ctx'  |~ e2 ~~~> ctx''  |~ eVal p ->
        true~ ctx'' |~ n / a / p ;= e3 ~~~> ctx''' |~ eVal v ->
          true~ ctx |~ eAttr e1 a e2 ~~~> ctx''' |~ eVal v

  | bst_attr_uncached: forall (b:bool) {P T:Type} (ctx ctx' ctx'' ctx''':context) (e1:exp node) (n:node) (e2:exp P) (p:P) (a:attr P T) (e3:exp T) (v:T),
      applycontext ctx'' P T n a p = e3 ->
      cached a = false \/ b = false \/ isVal e3 = true ->
        b~ ctx   |~ e1 ~~~> ctx'   |~ eVal n ->
        b~ ctx'  |~ e2 ~~~> ctx''  |~ eVal p ->
        b~ ctx'' |~ e3 ~~~> ctx''' |~ eVal v ->
          b~ ctx |~ eAttr e1 a e2 ~~~> ctx''' |~ eVal v

  (* Cache *)
  | bst_cache: forall (b:bool) {P T:Type} (ctx ctx':context) (n:node) (p:P) (a:attr P T) (e:exp T) (v:T),
      b~ ctx |~ e ~~~> ctx' |~ eVal v ->
        b~ ctx |~ n / a / p ;= e ~~~> ctx' ^^ (n, a, p, v) |~ eVal v

where "b '~' c1 '|~' e1 '~~~>' c2 '|~' e2" := (bigstep b c1 e1 c2 e2).
Hint Constructors bigstep.


(* this useful tactic clears a goal with a hypothesis that is an impossible step *)
Ltac step_contra :=
  simpl in *;
  match goal with
    | H:(step _ _ (eVal _) _ _) |- _  => inversion H
  end.





(*|                                  _   _
| |  _ __  _ __ ___  _ __   ___ _ __| |_(_) ___  ___
| | | '_ \| '__/ _ \| '_ \ / _ \ '__| __| |/ _ \/ __|
| | | |_) | | | (_) | |_) |  __/ |  | |_| |  __/\__ \
| | | .__/|_|  \___/| .__/ \___|_|   \__|_|\___||___/
| | |_|             |_|
|*)


Theorem step_determinism: forall {b:bool} {T:Type} {ctx c1 c2:context} {e e1 e2:exp T},
  b ~ ctx |~ e ~~> c1 |~ e1 ->
  b ~ ctx |~ e ~~> c2 |~ e2 ->
    e1 = e2 /\ c1 = c2.
Proof.
  intros b T ctx c1 c2 e e1 e2 H1 H2.
  generalize dependent c2. generalize dependent e2.
  induction H1; intros e2' c2' H2; inversion H2;
    substT2; expand_ands; subst; auto. (* creates 28 subgoals *)
  all: try (step_contra) (*20*).
  all: try (solve [apply IHstep in H8; expand_ands; subst; auto]) (*2*).
  all: try (solve [apply IHstep in H9; expand_ands; subst; auto]) (*3*).
  all: try (solve [apply IHstep in H10; expand_ands; subst; auto]) (*1*).
  decompose [or] H0; congruence.
  decompose [or] H13; congruence.
Qed.


Theorem step_progress: forall {b:bool} {T:Type} (e:exp T) (ctx:context),
  value e \/ (exists ctx' e', b ~ ctx |~ e ~~> ctx' |~ e').
Proof.
  intros.
  induction e; auto; right.

  (* eCond *)
  - decompose [or] IHe1.
    + inversion H; substT2. destruct v.
      * exists ctx. exists e2. constructor.
      * exists ctx. exists e3. constructor.
    + inversion H as [ctx' [e' H'] ]. exex. econstructor. apply H'.

  (* eApp *)
  - inversion_clear IHe1. inversion_clear IHe2.
    + inversion H; inversion H0; subst; substT2.
      * exex. apply st_appapp.
    + inversion H0 as [ctx' [e' H'] ]. exists ctx'. exists (e1 OF e').
      inversion H; substT2. eapply st_appright. apply H'.
    + inversion H as [ctx' [e' H'] ]. exex. eapply st_appleft. apply H'.

  (* eAttr *)
  - decompose [or] IHe1.
    + inversion H; substT2. decompose [or] IHe2.
      * inversion H0; substT2. exists ctx. destruct (cached a) eqn:Hcha; destruct b; destruct (isVal (applycontext ctx P T v a v0)) eqn:Hv.
        all: try(exists (applycontext ctx P T v a v0); apply st_attrapp_uncached; tauto).
        ** exists (v/a/v0 ;= applycontext ctx P T v a v0). apply st_attrapp_cached; auto.
      * destruct H0 as [ctx' [e2' H']]. exex. apply st_attr_parstep; auto. apply H'.
    + inversion H as [ctx' [e1' H'] ]. exex. apply st_attr_nodestep; auto. apply H'.

  (* eCache *)
  - decompose [or] IHe.
    + inversion H; substT2. exex. eapply st_cachewrite.
    + inversion_clear H as [ctx' [e' b0'] ]. exex. eapply st_cacheleft. apply b0'.
Qed.


Theorem multistep_determinism: forall {b:bool} {T:Type} (e:exp T) (ctx ctx1 ctx2:context) (v1 v2:T),
  b~ ctx |~ e ~~>* ctx1 |~ eVal v1 ->
  b~ ctx |~ e ~~>* ctx2 |~ eVal v2 ->
  v1 = v2 /\ ctx1 = ctx2.
Proof.
  intros b T e ctx c1 c2 v1 v2 H1 H2.
  generalize dependent c2. generalize dependent v2.
  dependent induction H1; intros v2 c2 H2; inversion H2; substT2; auto.
  - inversion H0;substT2. auto.
  - step_contra.
  - step_contra.
  - apply (step_determinism H) in H7. expand_ands. subst.
    apply IHmultistep with v1 _ _ in H9; auto.
Qed.












