
(** PicoJava Name Analysis in Saiga **)

Require Import Coq.Arith.Arith.
Require Import Coq.Bool.Bool.
Require Import Coq.Lists.List.
Require Export saiga.

Delimit Scope string_scope with string.
Local Open Scope string_scope.



(*|  _          _
| | | |__   ___| |_ __   ___ _ __ ___
| | | '_ \ / _ \ | '_ \ / _ \ '__/ __|
| | | | | |  __/ | |_) |  __/ |  \__ \
| | |_| |_|\___|_| .__/ \___|_|  |___/
| |              |_|
|*)

(* helpful notations *)
(* concatenate two node lists *)
Notation "la '+++' lb" := (eVal (@app node) OF la OF lb) (at level 40, left associativity).

(* IFFOK returns ex if (bf ex) holds, otherwise ef *)
Notation "'IFFOK' bf 'RETURN' ex 'ELSE' ef" := (IFF (eVal bf OF ex) THEN (ex) ELSE (ef)) (at level 40, left associativity, only parsing).

(* a pair of values, from expressions *)
Notation "'(' a ',,' b ')'" := (eVal pair OF a OF b).

(* notations for updated contexts *)
Notation "c '++' a '//' af" := (replace_afun c a af) (at level 36, no associativity).
Notation "c '++' n '\\' a '\\' p '\\' t" := (replace_node c n a p t) (at level 36, no associativity).





(* the 'null' node is used as a default for some attributes *)
Definition nNull : node := Node (NKind 0) 0.

(* some sugar for different kinds of attributes (implicit/parameterised/regular) *)
Definition iAttr (T:Type)   (d:T)           (n:nat) := @Attr unit T d false n.
Definition pAttr (P T:Type) (d:T) (ch:bool) (n:nat) := @Attr P T d ch n.
Definition rAttr (T:Type)   (d:T) (ch:bool) (n:nat) := @Attr unit T d ch n.

(* some functions that are used in attribute definitions *)
Definition getor {A:Type} (d:A) (oa:option A) : A :=
  match oa with
    | Some a => a
    | None   => d
  end.
Definition beq_optnode (on1 on2:option node) : bool :=
  match on1, on2 with
    | Some n1, Some n2 => beq_node n1 n2
    | None, None       => true
    | _, _             => false
  end.
Definition isSome {A:Type} (o:option A) : bool :=
  match o with
    | Some _ => true
    | None   => false
  end.





(*|
| |   __ _ _ __ __ _ _ __ ___  _ __ ___   __ _ _ __
| |  / _` | '__/ _` | '_ ` _ \| '_ ` _ \ / _` | '__|
| | | (_| | | | (_| | | | | | | | | | | | (_| | |
| |  \__, |_|  \__,_|_| |_| |_|_| |_| |_|\__,_|_|
| |  |___/
|*)


(* the 'grammar' for picojava. this is the list of node kinds
   and intrinsic attributes used in a picojava program *)

(* below find groups of node kinds, and the attributes that
   are expected (not enforced) to be used on these kinds of nodes *)

Definition kProgram   := NKind                      01.
Definition pgItems    := iAttr (list node) nil      02.

Definition kClassDecl := NKind                      03.
Definition dName      := iAttr (string) ""          04. (* used for all decls *)
Definition cdBlock    := iAttr (node) nNull         05.
Definition cdUse      := iAttr (option node) None   06.

Definition kBlock     := NKind                      07.
Definition bkItems    := iAttr (list node) nil      08.

Definition kVarDecl   := NKind                      09.
(* dName is also used on VarDecls *)
Definition vdAccess   := iAttr (node) nNull         10.

Definition kAssign    := NKind                      11.
Definition asAccess   := iAttr (node) nNull         12.
Definition asExp      := iAttr (node) nNull         13.

Definition kUse       := NKind                      14.
Definition usName     := iAttr (string) ""          15.

Definition kDot       := NKind                      16.
Definition dtAccess   := iAttr (node) nNull         17.
Definition dtUse      := iAttr (node) nNull         18.

Definition kBoolLit   := NKind                      19.
Definition blVal      := iAttr (bool) false         20.

Definition kUnkDecl   := NKind                      21.
Definition kPrimDecl  := NKind                      22.

Definition parent     := iAttr (node) nNull         23. (* parent used on all nodes *)





(*-- Predefined nodes. --*)
Definition nUnkDecl    : node := Node kUnkDecl      924.
Definition nUnkClass   : node := Node kUnkDecl      925.
Definition nEmptyBlock : node := Node kBlock        926.

(* the following are 'pushed' into the context later in this file, to
   provide some 'global' values *)
Definition unknownIdent        := "unknown".
Definition booleanIdent        := "boolean".
Definition integerIdent        := "int".
Definition nUnknownDeclaration := Node kUnkDecl     927.
Definition nBooleanDeclaration := Node kPrimDecl    928.
Definition nIntegerDeclaration := Node kPrimDecl    929.
Definition getPredefinedTypeList : list node :=
  [nUnknownDeclaration; nBooleanDeclaration; nIntegerDeclaration].

(* some helper functions that make use of predefined nodes and kinds *)
Definition isUnknown  (n:node) : bool := isnk n kUnkDecl.
Definition notUnknown (n:node) : bool := negb (isnk n kUnkDecl).
Definition notUnkClass (n:node): bool := negb (beq_node n nUnkClass).










(*|        _   _        _ _           _
| |   __ _| |_| |_ _ __(_) |__  _   _| |_ ___  ___
| |  / _` | __| __| '__| | '_ \| | | | __/ _ \/ __|
| | | (_| | |_| |_| |  | | |_) | |_| | ||  __/\__ \
| |  \__,_|\__|\__|_|  |_|_.__/ \__,_|\__\___||___/
|*)

(* First we define some attribute 'labels', then we define some
   attribute 'functions', then we put them all together in a context function *)


(* attribute labels - COMMON *)
Definition finddecl      := pAttr  (string*list node)  node  nNull  false   101.
Definition isDotUse      := rAttr                      bool  false  true    102.
Definition isClassSuper  := rAttr                      bool  false  true    113.
Definition lk_tipe       := rAttr                      node  nNull  true    104.
Definition ev_tipe       := rAttr                      node  nNull  true    105.
Definition lk_superclass := rAttr                      node  nNull  true    106.
Definition ev_superclass := rAttr                      node  nNull  true    107.
Definition lk_decl       := rAttr                      node  nNull  true    108.
Definition ev_decl       := rAttr                      node  nNull  true    112.

(* attribute labels - LOOKUP METHOD *)
Definition lookup        := pAttr  string              node  nNull  true    109.
Definition localLookup   := pAttr  string              node  nNull  true    110.
Definition remoteLookup  := pAttr  string              node  nNull  true    111.

(* attribute labels - ENVIRONMENT METHOD *)
Definition cenv          := rAttr               (list node)  nil    true    113.
Definition env           := rAttr               (list node)  nil    true    114.







(* attribute definitions - COMMON *)

Definition finddecl_fun (n:node) (name_bstmts:string * list node): exp node :=
  let (name, bstmts) := name_bstmts in

  match bstmts with
    | h::r => IFF (eVal (beq_string name) OF (eVal h DOT dName)) THEN
                (eVal h)
              ELSE
                (eVal nNull DOT finddecl WITH eVal (name, r))

    | nil => eVal nUnkDecl
  end.
Hint Unfold finddecl_fun.

Definition isDotUse_fun (n:node) (_:unit) : exp bool :=
  IFF (eVal (isnk2 kDot) OF (eVal n DOT parent)) THEN
    ((eVal (beq_node n)) OF (eVal n DOT parent DOT dtUse))
  ELSE
    eVal false.
Hint Unfold isDotUse_fun.

Definition isClassSuper_fun (n:node) (_:unit) : exp bool :=
  IFF (eVal (isnk2 kClassDecl) OF (eVal n DOT parent)) THEN
    ((eVal (beq_optnode (Some n))) OF (eVal n DOT parent DOT cdUse))
  ELSE
    eVal false.
Hint Unfold isClassSuper_fun.

(* we define superclass and tipe generally, then specify them for each method *)
Definition superclass_fun (decl:attr unit node) (n:node) (_:unit) : exp (node) :=
  IFF (eVal (isnk n kClassDecl)) THEN
    IFF (eVal isSome OF (eVal n DOT cdUse)) THEN
        IFF (eVal (isnk2 kClassDecl) OF (eVal (getor nUnkDecl) OF (eVal n DOT cdUse) DOT decl)) THEN
          (eVal (getor nUnkDecl) OF (eVal n DOT cdUse) DOT decl)
        ELSE
          (eVal nUnkClass)
    ELSE
      (eVal nUnkClass)
  ELSE
    (eVal nUnkClass).
Definition ev_superclass_fun : node -> unit -> exp (node) := superclass_fun ev_decl.
Definition lk_superclass_fun : node -> unit -> exp (node) := superclass_fun lk_decl.
Hint Unfold ev_superclass_fun lk_superclass_fun superclass_fun.

Definition tipe_fun (decl tipe:attr unit node) (n:node) (_:unit) : exp (node) :=
  IFF (eVal (isnk n kClassDecl)) THEN
    (eVal n)
  ELSE IFF (eVal (isnk n kVarDecl)) THEN
      IFF (eVal (isnk2 kClassDecl) OF (eVal n DOT vdAccess DOT decl)) THEN
        (eVal n DOT vdAccess DOT decl)
      ELSE
        (eVal nUnkClass)
  ELSE
      (eVal nUnkClass).
Definition ev_tipe_fun : node -> unit -> exp (node) := tipe_fun ev_decl ev_tipe.
Definition lk_tipe_fun : node -> unit -> exp (node) := tipe_fun lk_decl lk_tipe.
Hint Unfold ev_tipe_fun lk_tipe_fun tipe_fun.







(* attribute definitions - ENVIRONMENT METHOD *)
Definition ev_decl_fun (n:node) (_:unit) : exp node :=
  IFF (eVal (isnk n kDot)) THEN
    (eVal n DOT dtUse DOT ev_decl)

  ELSE IFF (eVal (isnk n kUse)) THEN
    IFF (eVal n DOT isDotUse) THEN
      (eVal nNull DOT finddecl WITH (eVal n DOT usName ,, eVal n DOT parent DOT dtAccess DOT ev_decl DOT ev_tipe DOT cdBlock DOT cenv))
    ELSE 
      (eVal nNull DOT finddecl WITH (eVal n DOT usName ,, eVal n DOT env))
  ELSE
    (eVal nUnkDecl).
Hint Unfold ev_decl_fun.

Definition env_fun (n:node) (_:unit) : exp (list node) :=
  IFF (eVal (isnk n kProgram)) THEN
    (eVal n DOT pgItems) +++ (eVal getPredefinedTypeList)
  ELSE IFF (eVal (isnk n kBlock)) THEN
    (eVal n DOT cenv) +++ (eVal n DOT parent DOT env)
  ELSE IFF (eVal (isnk n kUnkDecl)) THEN
    (eVal nil)
  ELSE
    (eVal n DOT parent DOT env).
Hint Unfold env_fun.

Definition cenv_fun (n:node) (_:unit) : exp (list node) :=
  IFF (eVal (isnk n kProgram)) THEN
    (eVal nil)
  ELSE IFF (eVal (isnk n kBlock)) THEN
      IFF (eVal notUnkClass OF (eVal n DOT parent DOT ev_superclass)) THEN
        (eVal n DOT bkItems) +++ ((eVal n DOT parent DOT ev_superclass) DOT cdBlock DOT cenv)
      ELSE
        (eVal n DOT bkItems)
  ELSE IFF (eVal (isnk n kUnkDecl)) THEN
    (eVal nil)
  ELSE
    (eVal n DOT parent DOT cenv).
Hint Unfold cenv_fun.





(* attribute definitions - LOOKUP METHOD *)

Definition lk_decl_fun (n:node) (_:unit) : exp node :=
  IFF (eVal (isnk n kDot)) THEN
    (eVal n DOT dtUse DOT lk_decl)
  ELSE IFF (eVal (isnk n kUse)) THEN
    (eVal n DOT lookup WITH (eVal n DOT usName))
  ELSE
    (eVal nUnkDecl).
Hint Unfold lk_decl_fun.

Definition lookup_fun (n:node) (s:string) : exp node :=
  IFF (eVal (isnk n kProgram)) THEN
    (eVal n DOT localLookup WITH eVal s)
  ELSE IFF (eVal (isnk n kBlock)) THEN
    IFFOK notUnknown RETURN (eVal n DOT localLookup WITH eVal s)
      ELSE IFFOK notUnknown RETURN (eVal n DOT parent DOT lk_superclass DOT cdBlock DOT remoteLookup WITH eVal s)
        ELSE (eVal n DOT parent DOT lookup WITH eVal s)
  ELSE (IFF (eVal (isnk n kUse)) THEN
    IFF (eVal n DOT isDotUse) THEN
        (eVal n DOT parent DOT dtAccess DOT lk_decl DOT lk_tipe DOT cdBlock DOT remoteLookup WITH eVal s)
    ELSE
      (eVal n DOT parent DOT lookup WITH eVal s)
  ELSE (IFF (eVal (isnk n kUnkDecl)) THEN
    (eVal nUnkDecl)
  ELSE
    (eVal n DOT parent DOT lookup WITH eVal s))).
Hint Unfold lookup_fun.

Definition localLookup_fun (n:node) (s:string) : exp node :=
  IFF (eVal (isnk n kProgram)) THEN
    IFFOK notUnknown RETURN (eVal nNull DOT finddecl WITH ((eVal s) ,, (eVal n DOT pgItems)))
    ELSE (eVal nNull DOT finddecl WITH ((eVal s) ,, (eVal getPredefinedTypeList)))
  ELSE IFF (eVal (isnk n kBlock)) THEN
    (eVal nNull DOT finddecl WITH ((eVal s) ,, (eVal n DOT bkItems)))
  ELSE
    (eVal nUnkDecl).
Hint Unfold localLookup_fun.

Definition remoteLookup_fun (n:node) (s:string) : exp node :=
  IFF (eVal (isnk n kBlock)) THEN

    IFFOK notUnknown RETURN (eVal n DOT localLookup WITH eVal s)
    ELSE IFF (eVal notUnkClass OF (eVal n DOT parent DOT lk_superclass)) THEN
        IFFOK notUnknown RETURN (eVal n DOT parent DOT lk_superclass DOT cdBlock DOT remoteLookup WITH eVal s)
          ELSE (eVal nUnkDecl)
      ELSE
        (eVal nUnkDecl)
  ELSE
    (eVal nUnkDecl).
Hint Unfold remoteLookup_fun.




(* attribute definition 'loaders'. these 'write' the appropriate
   attribute definitions into a given context. *)
Definition pjctx_lookup_attrsonly : context -> context := fun c => c
               ++ lk_decl          // lk_decl_fun
               ++ lk_tipe          // lk_tipe_fun
               ++ lk_superclass    // lk_superclass_fun
               ++ isDotUse         // isDotUse_fun
               ++ isClassSuper     // isClassSuper_fun
               ++ finddecl         // finddecl_fun
               ++ lookup           // lookup_fun
               ++ localLookup      // localLookup_fun
               ++ remoteLookup     // remoteLookup_fun.

Definition pjctx_enviro_attrsonly : context -> context := fun c => c
               ++ ev_decl          // ev_decl_fun
               ++ ev_tipe          // ev_tipe_fun
               ++ ev_superclass    // ev_superclass_fun
               ++ isDotUse         // isDotUse_fun
               ++ isClassSuper     // isClassSuper_fun
               ++ finddecl         // finddecl_fun
               ++ env              // env_fun
               ++ cenv             // cenv_fun.











(*|                                 _
| |   _____  ____ _ _ __ ___  _ __ | | ___
| |  / _ \ \/ / _` | '_ ` _ \| '_ \| |/ _ \
| | |  __/>  < (_| | | | | | | |_) | |  __/
| |  \___/_/\_\__,_|_| |_| |_| .__/|_|\___|
| |                          |_|
|*)

(* Here we define an actual AST of a PicoJava program (shown below),
   by way of a context function that returns the appropriate values
   given the appropriate inputs. All of the Coq in this section was
   generated from a Scala script, but can be defined manually *)


(* class A {
    int y;
    AA a;
    y = a.x;
    class AA {
        int x;
    }
    class BB extends AA {
        BB b;
        b.y = b.x;
    }
} *)

(* these are all the nodes in the above example *)
Definition nProg_1         := Node kProgram     1.
Definition nClassDecl_2    := Node kClassDecl   2. (* A *)
Definition nBlock_3        := Node kBlock       3.
Definition nVarDecl_4      := Node kVarDecl     4. (* y *)
Definition nUse_5          := Node kUse         5.
Definition nVarDecl_6      := Node kVarDecl     6. (* a *)
Definition nUse_7          := Node kUse         7.
Definition nAssign_8       := Node kAssign      8.
Definition nUse_9          := Node kUse         9.
Definition nDot_10         := Node kDot         10.
Definition nUse_11         := Node kUse         11.
Definition nUse_12         := Node kUse         12.
Definition nClassDecl_13   := Node kClassDecl   13. (* AA *)
Definition nBlock_14       := Node kBlock       14.
Definition nVarDecl_15     := Node kVarDecl     15. (* x *)
Definition nUse_16         := Node kUse         16.
Definition nClassDecl_17   := Node kClassDecl   17. (* BB *)
Definition nBlock_18       := Node kBlock       18.
Definition nVarDecl_19     := Node kVarDecl     19. (* b *)
Definition nUse_20         := Node kUse         20.
Definition nAssign_21      := Node kAssign      21.
Definition nDot_22         := Node kDot         22.
Definition nUse_23         := Node kUse         23.
Definition nUse_24         := Node kUse         24.
Definition nDot_25         := Node kDot         25.
Definition nUse_26         := Node kUse         26.
Definition nUse_27         := Node kUse         27.
Definition nUse_28         := Node kUse         28.


(* these are all the intrinsic properties of these nodes, as well as
   the predefinedtypelist info and unknown class definition. *)
Definition write_properties (c:context): context := c
 ++ nUnknownDeclaration \\ dName   \\  tt  \\  "$unknown"
 ++ nBooleanDeclaration \\ dName   \\  tt  \\  "boolean"
 ++ nIntegerDeclaration \\ dName   \\  tt  \\  "int"
 ++ nUnkClass           \\ cdBlock \\  tt  \\  nEmptyBlock
 ++ nUnkClass           \\ cdUse   \\  tt  \\  None
 ++ nEmptyBlock         \\ bkItems \\  tt  \\  nil
 ++ nEmptyBlock         \\ parent  \\  tt  \\ nUnkClass

 ++ nProg_1           \\ pgItems      \\ tt \\   [nClassDecl_2]
 ++ nClassDecl_2      \\ dName        \\ tt \\   "A"
 ++ nClassDecl_2      \\ cdUse        \\ tt \\   None
 ++ nClassDecl_2      \\ cdBlock      \\ tt \\   nBlock_3
 ++ nBlock_3          \\ bkItems      \\ tt \\   [nVarDecl_4; nVarDecl_6; nAssign_8; nClassDecl_13; nClassDecl_17]
 ++ nVarDecl_4        \\ vdAccess     \\ tt \\   nUse_5
 ++ nVarDecl_4        \\ dName        \\ tt \\   "y"
 ++ nUse_5            \\ usName       \\ tt \\   "int"
 ++ nVarDecl_6        \\ vdAccess     \\ tt \\   nUse_7
 ++ nVarDecl_6        \\ dName        \\ tt \\   "a"
 ++ nUse_7            \\ usName       \\ tt \\   "AA"
 ++ nAssign_8         \\ asAccess     \\ tt \\   nUse_9
 ++ nAssign_8         \\ asExp        \\ tt \\   nDot_10
 ++ nUse_9            \\ usName       \\ tt \\   "x"
 ++ nDot_10           \\ dtAccess     \\ tt \\   nUse_11
 ++ nDot_10           \\ dtUse        \\ tt \\   nUse_12
 ++ nUse_11           \\ usName       \\ tt \\   "a"
 ++ nUse_12           \\ usName       \\ tt \\   "x"
 ++ nClassDecl_13     \\ dName        \\ tt \\   "AA"
 ++ nClassDecl_13     \\ cdUse        \\ tt \\   None
 ++ nClassDecl_13     \\ cdBlock      \\ tt \\   nBlock_14
 ++ nBlock_14         \\ bkItems      \\ tt \\   [nVarDecl_15]
 ++ nVarDecl_15       \\ vdAccess     \\ tt \\   nUse_16
 ++ nVarDecl_15       \\ dName        \\ tt \\   "x"
 ++ nUse_16           \\ usName       \\ tt \\   "int"
 ++ nClassDecl_17     \\ dName        \\ tt \\   "BB"
 ++ nClassDecl_17     \\ cdUse        \\ tt \\   Some(nUse_28)
 ++ nUse_28           \\ usName       \\ tt \\   "AA"
 ++ nClassDecl_17     \\ cdBlock      \\ tt \\   nBlock_18
 ++ nBlock_18         \\ bkItems      \\ tt \\   [nVarDecl_19; nAssign_21]
 ++ nVarDecl_19       \\ vdAccess     \\ tt \\   nUse_20
 ++ nVarDecl_19       \\ dName        \\ tt \\   "b"
 ++ nUse_20           \\ usName       \\ tt \\   "BB"
 ++ nAssign_21        \\ asAccess     \\ tt \\   nDot_22
 ++ nAssign_21        \\ asExp        \\ tt \\   nDot_25
 ++ nDot_22           \\ dtAccess     \\ tt \\   nUse_23
 ++ nDot_22           \\ dtUse        \\ tt \\   nUse_24
 ++ nUse_23           \\ usName       \\ tt \\   "b"
 ++ nUse_24           \\ usName       \\ tt \\   "y"
 ++ nDot_25           \\ dtAccess     \\ tt \\   nUse_26
 ++ nDot_25           \\ dtUse        \\ tt \\   nUse_27
 ++ nUse_26           \\ usName       \\ tt \\   "b"
 ++ nUse_27           \\ usName       \\ tt \\   "x"
 ++ nClassDecl_2      \\ parent       \\ tt \\   nProg_1
 ++ nBlock_3          \\ parent       \\ tt \\   nClassDecl_2
 ++ nVarDecl_4        \\ parent       \\ tt \\   nBlock_3
 ++ nUse_5            \\ parent       \\ tt \\   nVarDecl_4
 ++ nVarDecl_6        \\ parent       \\ tt \\   nBlock_3
 ++ nUse_7            \\ parent       \\ tt \\   nVarDecl_6
 ++ nAssign_8         \\ parent       \\ tt \\   nBlock_3
 ++ nUse_9            \\ parent       \\ tt \\   nAssign_8
 ++ nDot_10           \\ parent       \\ tt \\   nAssign_8
 ++ nUse_11           \\ parent       \\ tt \\   nDot_10
 ++ nUse_12           \\ parent       \\ tt \\   nDot_10
 ++ nClassDecl_13     \\ parent       \\ tt \\   nBlock_3
 ++ nBlock_14         \\ parent       \\ tt \\   nClassDecl_13
 ++ nVarDecl_15       \\ parent       \\ tt \\   nBlock_14
 ++ nUse_16           \\ parent       \\ tt \\   nVarDecl_15
 ++ nClassDecl_17     \\ parent       \\ tt \\   nBlock_3
 ++ nUse_28           \\ parent       \\ tt \\   nClassDecl_17
 ++ nBlock_18         \\ parent       \\ tt \\   nClassDecl_17
 ++ nVarDecl_19       \\ parent       \\ tt \\   nBlock_18
 ++ nUse_20           \\ parent       \\ tt \\   nVarDecl_19
 ++ nAssign_21        \\ parent       \\ tt \\   nBlock_18
 ++ nDot_22           \\ parent       \\ tt \\   nAssign_21
 ++ nUse_23           \\ parent       \\ tt \\   nDot_22
 ++ nUse_24           \\ parent       \\ tt \\   nDot_22
 ++ nDot_25           \\ parent       \\ tt \\   nAssign_21
 ++ nUse_26           \\ parent       \\ tt \\   nDot_25
 ++ nUse_27           \\ parent       \\ tt \\   nDot_25.


(* Write the 'extrinsic' attribtues to the contexts already defined above. *)
Definition lookup_ctx := write_properties (pjctx_lookup_attrsonly context_baseline).
Definition enviro_ctx := write_properties (pjctx_enviro_attrsonly context_baseline).





(*|  _            _
| | | |_ ___  ___| |_ ___
| | | __/ _ \/ __| __/ __|
| | | ||  __/\__ \ |_\__ \
| |  \__\___||___/\__|___/
|*)

(* autostep is a complex set of tactics that automatically prove goals
   that involve stepping a Saiga expression. *)
Require Import autostep.

(* note: autostep is very finnicky. All the right things need to be set up
   with "Hint Unfold" etc, and autostep is only set up to deal with types
   that have been considered so far. All of the below runs on my setup right now
   (CoqIDE on OSX), and has been tested in some other environments, but there
   are no guarantees that autostep will work on any new inputs.

   All of the below could be proven manually, but the proofs would be *very* long.
   If you're having any trouble feel free to contact me at scott.buckley@mq.edu.au *)





(* used essentially to check that autostep is working *)
Example basic_test_0:
  lookup_ctx ||- IFF eVal true THEN eVal nUse_12 DOT usName ELSE eVal "cats" -->* lookup_ctx ||- eVal "x".
Proof.
  time autostep_all.
Qed.

(* class members are resolved (lookup and enviro, uncached and cached) *)
Example picojava_test1_uncached_lookup:
  lookup_ctx ||- eVal nUse_12 DOT lk_decl -->* lookup_ctx ||- eVal nVarDecl_15.
Proof.
  time autostep_all. (* 250 steps *)
Qed.

Example picojava_test1_uncached_enviro:
  enviro_ctx ||- eVal nUse_12 DOT ev_decl -->* enviro_ctx ||- eVal nVarDecl_15.
Proof.
  time autostep_all. (* 235 steps *)
Qed.

Example picojava_test1_cached_lookup:
  exists lookup_ctx', lookup_ctx |= eVal nUse_12 DOT lk_decl ==>* lookup_ctx' |= eVal nVarDecl_15.
Proof.
  eexists.

  time autostep_all. (* 156 steps *)
Qed. 

Example picojava_test1_cached_enviro:
  exists enviro_ctx', enviro_ctx |= eVal nUse_12 DOT ev_decl ==>* enviro_ctx' |= eVal nVarDecl_15.
Proof.
  eexists. time autostep_all. (* 163 steps*)
Qed.

(* this test is mostly for finddecl and lists *)
Example picojava_basic_test_2:
  lookup_ctx ||- eVal nNull DOT finddecl WITH eVal ("AA", [nClassDecl_13; nClassDecl_17]) -->* lookup_ctx ||- eVal nClassDecl_13.
Proof.
  autostep_lg.
  autostep_lg.
  autostep_lg.
  autostep_lg.
  autostep_lg.
Qed.


(* nested classes are resolved *)
Example picojava_test2_uncached_lookup:
  lookup_ctx ||- eVal nUse_20 DOT lk_decl -->* lookup_ctx ||- eVal nClassDecl_17.
Proof.
  time autostep_all. (* 280 steps, 78 seconds [qed/osx]*)
Qed.

Example picojava_test2_uncached_enviro:
  enviro_ctx ||- eVal nUse_20 DOT ev_decl -->* enviro_ctx ||- eVal nClassDecl_17.
Proof.
  time autostep_all. (* 397 steps, 145 seconds [qed/osx]*)
Qed.


Example picojava_test2_cached_lookup:
  exists ctx',
  lookup_ctx |= eVal nUse_20 DOT lk_decl ==>* ctx' |= eVal nClassDecl_17.
Proof.
  eexists. time autostep_all. (* xx steps, xx seconds [qed/osx]*)
Qed.

Example picojava_test2_cached_enviro:
  exists ctx',
  enviro_ctx |= eVal nUse_20 DOT ev_decl ==>* ctx' |= eVal nClassDecl_17.
Proof.
  eexists. time autostep_all. (* xx steps, xx seconds [qed/osx]*)
Qed.




(* nested names hide outer one *)
Example picojava_test3_uncached_lookup:
  lookup_ctx ||- eVal nUse_27 DOT lk_decl -->* lookup_ctx ||- eVal nVarDecl_15.
Proof.
  time autostep_all. (* 1163 steps *)
Qed.

Example picojava_test3_uncached_enviro:
  enviro_ctx ||- eVal nUse_27 DOT ev_decl -->* enviro_ctx ||- eVal nVarDecl_15.
Proof.
  time autostep_all. (* 1512 steps *)
Qed.

Example picojava_test3_cached_lookup:
  exists ctx',
  lookup_ctx |= eVal nUse_27 DOT lk_decl ==>* ctx' |= eVal nVarDecl_15.
Proof.
  eexists. time autostep_all. (* xx steps *)
Qed.

Example picojava_test3_cached_enviro:
  exists ctx',
  enviro_ctx |= eVal nUse_27 DOT ev_decl ==>* ctx' |= eVal nVarDecl_15.
Proof.
  eexists. time autostep_all. (* xx steps *)
Qed.




(* non-members in scope are not resolved as members *)
Example picojava_test4_uncached_lookup:
  lookup_ctx ||- eVal isUnknown OF (eVal nUse_24 DOT lk_decl) -->* lookup_ctx ||- eVal true.
Proof.
  time autostep_all. (* 988 steps *)
Qed.

Example picojava_test4_uncached_enviro:
  enviro_ctx ||- eVal isUnknown OF (eVal nUse_24 DOT ev_decl) -->* enviro_ctx ||- eVal true.
Proof.
  time autostep_all. (* xx steps *)
Qed.

Example picojava_test4_cached_lookup:
  exists ctx',
  lookup_ctx |= eVal isUnknown OF (eVal nUse_24 DOT lk_decl) ==>* ctx' |= eVal true.
Proof.
  eexists. time autostep_all. (* xx steps *)
Qed.

Example picojava_test4_cached_enviro:
  exists ctx',
  enviro_ctx |= eVal isUnknown OF (eVal nUse_24 DOT ev_decl) ==>* ctx' |= eVal true.
Proof.
  eexists. time autostep_all. (* xx steps *)
Qed.

































